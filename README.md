# Lorem Ipsum Restaurant

## About

* Developer: Biesmans Carmen

## Requirements

* HTML5, CSS3, Vanilla JavaScript
* ITCSS principe in combination with BEM and Sass as preprocessor
* Grid: flexbox and/or css native grid

## Notes

* A robots.txt and robots meta tag have been added to prevent the website from showing up in search engines