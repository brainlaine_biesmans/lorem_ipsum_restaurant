addEventListener("load", init);

function init() {
    const form = document.querySelector(".footer-subscribe__form");
    document.querySelector("#email-subscribe").addEventListener("change", validateSubscribeEmail);
    form.addEventListener("submit", validateSubscribeForm);
}

function validateSubscribeEmail() {
    const contentEmail = document.querySelector("#email-subscribe").value;
    const feedback = document.querySelector(".footer-subscribe__email-error");
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(contentEmail)) {
        feedback.innerHTML = "Het e-mail adres dat u heeft ingegeven is niet correct.";
        return false;
    } else {
        feedback.innerHTML = "";
    }
    return true;
}

function subscribeContentIsEmpty() {
    const contentEmail = document.querySelector("#email-subscribe").value;
    return (contentEmail.empty());
}

function validateSubscribeForm(event) {
    const emailOk = validateSubscribeEmail();
    const feedback = document.querySelector(".footer-subscribe__form-error");
    if (!emailOk || subscribeContentIsEmpty()) {
        feedback.innerHTML = "Gelieve alle velden correct in te vullen.";
        event.preventDefault();
    } else {
        feedback.innerHTML = "";
    }
}