addEventListener("load", init);

function init() {
    changeOpeningHoursToday();
}

function changeOpeningHoursToday() {
    const now = new Date();
    const opens = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 11, 0);
    const closes = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 22, 0);

    if (now >= opens && now < closes) {
        createOpeningHoursToday(" Wij zijn vandaag ", "open tot 22u");
    } else {
        createOpeningHoursToday(" Wij zijn momenteel gesloten, morgen zijn wij weer ", "open van 11u tot 22u");
    }
}

function createOpeningHoursToday(regularTextContent, highlightedTextContent) {
    const openingHoursTodayText = document.querySelector(".info-opening-hours-today__text-container");
    const textSpanElem = document.createElement("span");
    textSpanElem.setAttribute("class", "info-opening-hours-today__text-part info-opening-hours-today__text-part--color-neutral info-opening-hours-today__text-part--underline");
    const regularText = document.createTextNode(regularTextContent);
    const highlightedText = document.createTextNode(highlightedTextContent);

    openingHoursTodayText.innerHTML = "";
    openingHoursTodayText.appendChild(regularText);
    textSpanElem.appendChild(highlightedText);
    openingHoursTodayText.appendChild(textSpanElem);
}