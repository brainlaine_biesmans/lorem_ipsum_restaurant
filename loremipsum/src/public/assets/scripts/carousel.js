addEventListener("load", init);

function init() {
    carousel();
}

function carousel() {
    const banner = document.querySelector(".banner");
    const images = [
        "assets/images/components/carousel/carousel01.jpg",
        "assets/images/components/carousel/carousel02.jpg",
        "assets/images/components/carousel/carousel03.jpg",
        "assets/images/components/carousel/carousel04.jpg",
        "assets/images/components/carousel/carousel05.jpg",
        "assets/images/components/carousel/carousel06.jpg"
    ];

    let currentIndex = 0;

    function nextImage() {
        currentIndex = (currentIndex + 1) % images.length;
        banner.style.backgroundImage = `linear-gradient(to right, rgb(255, 255, 255), transparent), url("${images[currentIndex]}")`;
    }

    setInterval(nextImage, 3000);
}