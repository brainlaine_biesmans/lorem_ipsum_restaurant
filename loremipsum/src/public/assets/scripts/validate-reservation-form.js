addEventListener("load", init);

function init() {
    const form = document.querySelector(".reservation-online__form");
    document.querySelector("#email").addEventListener("change", validateEmail);
    document.querySelector("#date").addEventListener("change", validateDate);
    document.querySelector("#time").addEventListener("change", validateTime);
    document.querySelector("#pers").addEventListener("change", validatePers);
    form.addEventListener("submit", validateForm);
}

function validateEmail() {
    const contentEmail = document.querySelector("#email").value;
    const feedback = document.querySelector(".reservation__email-error");
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(contentEmail)) {
        feedback.innerHTML = "Het e-mail adres dat u heeft ingegeven is niet correct.";
        return false;
    } else {
        feedback.innerHTML = "";
    }
    return true;
}

function validateDate() {
    const contentDate = document.querySelector("#date").value;
    const feedback = document.querySelector(".reservation__date-error");
    const enteredDate = new Date(contentDate)
    enteredDate.setHours(0, 0, 0, 0);
    const today = new Date();
    today.setHours(0, 0, 0, 0);

    if (enteredDate <= today) {
        feedback.innerHTML = "Gelieve een datum in de toekomst te kiezen.";
        return false;
    } else {
        feedback.innerHTML = "";
    }
    return true;
}

function validateTime() {
    const contentTime = document.querySelector("#time").value;
    const feedback = document.querySelector(".reservation__time-error");
    const [hour, minute] = contentTime.split(':');
    const now = new Date();
    const enteredTime = new Date(now.getFullYear(), now.getMonth(), now.getDate(), hour, minute);
    const opens = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 11, 0);
    const closes = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 21, 0);

    if (enteredTime < opens || enteredTime > closes) {
        feedback.innerHTML = "Gelieve een tijdstip te kiezen tussen 11:00 en 21:00.";
        return false;
    } else {
        feedback.innerHTML = "";
    }
    return true;
}

function validatePers() {
    const contentPers = document.querySelector("#pers").value;
    const feedback = document.querySelector(".reservation__pers-error");
    const min = 1;
    const max = 80;
    if (contentPers > max || contentPers < min) {
        feedback.innerHTML = "Gelieve een aantal in te geven tussen 1 en 80.";
        return false;
    } else {
        feedback.innerHTML = "";
    }
    return true;
}

function contentIsEmpty() {
    const contentEmail = document.querySelector("#email").value;
    const contentDate = document.querySelector("#date").value;
    const contentTime = document.querySelector("#time").value;
    const contentPers = document.querySelector("#pers").value;
    return (contentEmail.empty() || contentDate.empty() || contentTime.empty() || contentPers.empty());
}

function validateForm(event) {
    const emailOk = validateEmail();
    const dateOk = validateDate();
    const timeOk = validateTime();
    const persOk = validatePers();
    const feedback = document.querySelector(".reservation__form-error");
    if (!emailOk || !dateOk || !timeOk || !persOk || contentIsEmpty()) {
        feedback.innerHTML = "Gelieve alle velden correct in te vullen.";
        event.preventDefault();
    } else {
        feedback.innerHTML = "Uw reservatie aanvraag is verzonden, gelieve te wachten op bevestiging van het restaurant.";
    }
}